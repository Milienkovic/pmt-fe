import React from 'react';
import './App.css';
import Dashboard from "./components/Dashboard";
import Header from "./components/layout/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import AddProject from "./components/project/AddProject";
import {Provider} from "react-redux";
import store from "./store/store";
import UpdateProject from "./components/project/UpdateProject";
import ProjectBoard from "./components/projectBoard/ProjectBoard";
import AddProjectTask from "./components/projectBoard/projectTasks/AddProjectTask";
import UpdateProjectTask from "./components/projectBoard/projectTasks/UpdateProjectTask";
import Landing from "./components/layout/Landing";
import Register from "./components/userManagement/Register";
import Login from "./components/userManagement/Login";
import jwt_decode from "jwt-decode";
import setJWTToken from "./utils/setJWTToken";
import {SET_CURRENT_USER} from "./actions/types";
import {logout} from "./actions/securityActions";
import SecureRoute from "./utils/secureRoute";

const jwtToken = localStorage.jwtToken;
if (jwtToken) {
    setJWTToken(jwtToken);
    const decoded_jwtToken = jwt_decode(jwtToken);
    store.dispatch({
        type: SET_CURRENT_USER,
        payload: decoded_jwtToken
    });

    const currentTime = Date.now() / 1000;
    console.log(currentTime);
    console.log(decoded_jwtToken.exp + "   exp");

    if (decoded_jwtToken.exp < currentTime) {
        store.dispatch(logout());
        window.location.href = "/";
    }

}


function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <div className="App">
                    <Header/>
                    {/*public*/}
                    <Route exact path="/" component={Landing}/>
                    <Route exact path="/registration" component={Register}/>
                    <Route exact path="/login" component={Login}/>

                    {/*private*/}
                    <Switch>
                        <SecureRoute exact path="/dashboard" component={Dashboard}/>
                        <SecureRoute exact path="/addProject" component={AddProject}/>
                        <SecureRoute exact path="/updateProject/:id" component={UpdateProject}/>
                        <SecureRoute exact path="/projectBoard/:id" component={ProjectBoard}/>
                        <SecureRoute exact path="/addProjectTask/:id" component={AddProjectTask}/>
                        <SecureRoute exact path="/updateProjectTask/:backlogId/:ptId" component={UpdateProjectTask}/>
                    </Switch>
                </div>
            </BrowserRouter>
        </Provider>
    );
}

export default App;

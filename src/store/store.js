import {createStore, applyMiddleware, compose} from "redux";
import thunk from "redux-thunk";
import rootReducer from "../reducers/";

const initialState = {};

const middleware = [thunk];

let store;
const REACT_REDUX_DEVTOOLS = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
if (REACT_REDUX_DEVTOOLS) {
    store = createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middleware),
            REACT_REDUX_DEVTOOLS));
} else {
    store = createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middleware)));
}


export default store;
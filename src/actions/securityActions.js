import axios from "axios";
import {GET_ERRORS, SET_CURRENT_USER} from "./types";
import setJWTToken from "../utils/setJWTToken";
import jwt_decode from "jwt-decode";

export const createNewUser = (newUser, history) => async dispatch => {
    await axios.post("/api/users/register", newUser)
        .then(_ => {
            history.push("/login");
            dispatch({
                type: GET_ERRORS,
                payload: {}
            })
        })
        .catch(e => {
            dispatch({
                type: GET_ERRORS,
                payload: e.response.data
            })
        })
};

export const login = loginRequest => async dispatch => {
    await axios.post("/api/users/login", loginRequest)
        .then(response => {
            const {token} = response.data;
            localStorage.setItem("jwtToken", token);
            setJWTToken(token);
            const decoded = jwt_decode(token);
            console.log(decoded)
            dispatch({
                type: SET_CURRENT_USER,
                payload: decoded
            });
            dispatch({
                type: GET_ERRORS,
                payload: {}
            });
        })
        .catch(e => {
            dispatch({
                type: GET_ERRORS,
                payload: e.response.data
            })
        })
};
export const logout = () => dispatch => {
    localStorage.removeItem("jwtToken");
    setJWTToken(false);
    dispatch({
        type: SET_CURRENT_USER,
        payload: {}
    });
};
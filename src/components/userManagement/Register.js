import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {createNewUser} from "../../actions/securityActions";
import classNames from "classnames";

class Register extends Component {
    state = {
        username: "",
        fullName: "",
        password: "",
        confirmPassword: "",
        errors: {}
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.errors){
            this.setState({errors:nextProps.errors})
        }
    }

    componentDidMount() {
        if (this.props.security.validToken){
            this.props.history.push("/dashboard");
        }
    }

    onSubmit = event => {
        event.preventDefault();
        const newUser = {
            username: this.state.username,
            fullName: this.state.fullName,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword
        };
        this.props.createNewUser(newUser, this.props.history);
    };

    onChange = event => {
        this.setState({[event.target.name]: event.target.value})
    };

    render() {
        return (
            <div className="register">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Sign Up</h1>
                            <p className="lead text-center">Create your Account</p>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="text" className={classNames("form-control form-control-lg",
                                        {"is-invalid": this.state.errors.fullName})} placeholder="Name"
                                           name="fullName"
                                           value={this.state.fullName}
                                           onChange={this.onChange}
                                           />
                                    {this.state.errors.fullName && (
                                        <div className="invalid-feedback">{this.state.errors.fullName}</div>)}
                                </div>
                                <div className="form-group">
                                    <input type="email" className={classNames("form-control form-control-lg",
                                        {"is-invalid": this.state.errors.username})}
                                           placeholder="Email Address" name="username"
                                           value={this.state.username}
                                           onChange={this.onChange}/>
                                    {this.state.errors.username && (
                                        <div className="invalid-feedback">{this.state.errors.username}</div>)}

                                </div>
                                <div className="form-group">
                                    <input type="password" className={classNames("form-control form-control-lg",
                                        {"is-invalid": this.state.errors.password})}
                                           placeholder="Password" name="password"
                                           value={this.state.password}
                                           onChange={this.onChange}/>
                                    {this.state.errors.password && (
                                        <div className="invalid-feedback">{this.state.errors.password}</div>)}
                                </div>
                                <div className="form-group">
                                    <input type="password" className={classNames("form-control form-control-lg",
                                        {"is-invalid": this.state.errors.confirmPassword})}
                                           placeholder="Confirm Password"
                                           name="confirmPassword"
                                           value={this.state.confirmPassword}
                                           onChange={this.onChange}/>
                                    {this.state.errors.confirmPassword && (
                                        <div className="invalid-feedback">{this.state.errors.confirmPassword}</div>)}
                                </div>
                                <input type="submit" className="btn btn-info btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Register.propTypes = {
    createNewUser: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    security: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    errors: state.errors,
    security: state.security
});

export default connect(mapStateToProps, {createNewUser})(Register);
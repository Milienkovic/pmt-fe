import React, {Component} from 'react';
import {Link} from "react-router-dom";
import classNames from "classnames";
import {connect} from "react-redux";
import {getProjectTask,updateProjectTask} from "../../../actions/backlogActions";
import PropTypes from "prop-types";

class UpdateProjectTask extends Component {
    state = {
        id: "",
        summary: "",
        acceptanceCriteria: "",
        status: "",
        priority: "",
        dueDate: "",
        projectIdentifier: "",
        projectSequence: "",
        errors: {}
    };

    componentDidMount() {
        const {backlogId, ptId} = this.props.match.params;
        this.props.getProjectTask(backlogId, ptId, this.props.history);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.errors) {
            this.setState({errors: nextProps.errors})
        }

        const {
            id,
            summary,
            acceptanceCriteria,
            status,
            priority,
            dueDate,
            projectIdentifier,
            projectSequence
        } = nextProps.project_task;
        this.setState({
            id,
            summary,
            acceptanceCriteria,
            status,
            priority,
            projectSequence,
            dueDate,
            projectIdentifier
        })
    }

    onChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    onSubmit = event => {
        event.preventDefault();
        const updatedProjectTask = {
            id: this.state.id,
            summary: this.state.summary,
            acceptanceCriteria: this.state.acceptanceCriteria,
            status: this.state.status,
            priority: this.state.priority,
            projectSequence: this.state.projectSequence,
            dueDate: this.state.dueDate,
            projectIdentifier: this.state.projectIdentifier
        };
       this.props.updateProjectTask(
           this.state.projectIdentifier,
           this.state.projectSequence,
           updatedProjectTask,
           this.props.history);
    };

    render() {
        return (
            <div className="add-PBI">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <Link to={`/projectBoard/${this.state.projectIdentifier}`} className="btn btn-light">
                                Back to Project Board
                            </Link>
                            <h4 className="display-4 text-center">Add /Update Project Task</h4>
                            <p className="lead text-center">Project Name: {this.state.projectIdentifier} + Project Task
                                ID: {this.state.projectSequence}</p>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="text" className={classNames("form-control form-control-lg",
                                        {"is-invalid": this.state.errors.summary})}
                                           name="summary"
                                           value={this.state.summary}
                                           onChange={this.onChange}
                                           placeholder="Project Task summary"/>
                                    {this.state.errors.summary && (
                                        <div className="invalid-feedback">{this.state.errors.summary}</div>)}
                                </div>
                                <div className="form-group">
                                    <textarea className="form-control form-control-lg" placeholder="Acceptance Criteria"
                                              value={this.state.acceptanceCriteria}
                                              onChange={this.onChange}
                                              name="acceptanceCriteria"/>
                                </div>
                                <h6>Due Date</h6>
                                <div className="form-group">
                                    <input type="date" className="form-control form-control-lg" name="dueDate"
                                           value={this.state.dueDate}
                                           onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <select className="form-control form-control-lg" name="priority"
                                            value={this.state.priority}
                                            onChange={this.onChange}>
                                        <option value={0}>Select Priority</option>
                                        <option value={1}>High</option>
                                        <option value={2}>Medium</option>
                                        <option value={3}>Low</option>
                                    </select>
                                </div>

                                <div className="form-group">
                                    <select className="form-control form-control-lg" name="status"
                                            value={this.state.status}
                                            onChange={this.onChange}>
                                        <option value="">Select Status</option>
                                        <option value="TO_DO">TO DO</option>
                                        <option value="IN_PROGRESS">IN PROGRESS</option>
                                        <option value="DONE">DONE</option>
                                    </select>
                                </div>

                                <input type="submit" className="btn btn-primary btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

UpdateProjectTask.propTypes = {
    getProjectTask: PropTypes.func.isRequired,
    project_task: PropTypes.object.isRequired,
    updateProjectTask: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    project_task: state.backlog.project_task,
    errors: state.errors
});

export default connect(mapStateToProps, {getProjectTask,updateProjectTask})(UpdateProjectTask);